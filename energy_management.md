# What are the activities you do that make you relax - Calm quadrant?

Following activities helps me stay calm.

- Listening songs.
- Playing games.
- Watching movies.

# When do you find getting into the Stress quadrant?

- Working continuously for 4 hours.
- When deadline for submissions of work are close.
- When there was a test.

# How do you understand if you are in the Excitement quadrant?

- Feeling happy, joyful.
- Having focus on particular task.

# Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep is super power which is not an optional.
- After observing two group of people with proper sleep and not proper sleep, it shows who sleep more have good immune system and good concentration.
- Proper schedule for sleep is required.
- While in deep sleep brain contains high electrical pulse which helps in permanenting the temporary memory.
- Correct amount sleep can reduce the heart problems.


# What are some ideas that you can implement to sleep better?

- Stoping use of mobile phone before going bed.
- Having a strict schedule for sleep.

# Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- It decreases the anxiety.
- It decreses the depression.
- It increases intelligence and grasping power.
- Increases the blood circulation to brain.
- Gives more time in concentration mode.
- Helps in creating new cells in brain.


# What are some steps you can take to exercise more?

- Adding exercise to daily routines.
- Setting short term goals.
