# What are the steps/strategies to do Active Listening

- Not to distract by other things.
- Focus on the speaker.
- Let them finish and response.
- Show you're interested and keep the other person talking.
- Show that you're listening with body language.
- Taking the notes during the conversation.

# According to Fisher's model, what are the key points of Reflective Listening

- Reflective listening is a sincere interest while listening to speaker.
- Understanding  the speaker words correctly and giving response back.
- Reflective listening could be an effective strategy to understand the other's problem, mindset etc..

# What are the obstacles in your listening process

- I would become easily distracted by sorrounded things.
- I'am unable to focus on the things efficiently.
- I would like to take breaks more than focusing period.

# What can you do to improve your listening

- Prefer to quite places.
- Setting the things as short-term goals.
- Taking the breaks while in once.

# When do you switch to Passive communication style in your day to day life

- For example, The task like assignment was given and had to complete it by EOD, but Iam unable to complete it within deadline and I'am hesitate to ask extra time. There I will switch to passive communication.
- Where I couldn't say no, I will switch to passive communication.

# When do you switch into Aggressive communication styles in your day to day life

- Expressing our feelings directly without considering other's opinion.
- Demaning the things I have wish for.
- In some places or things we should be aggressive to get things better.
- If I feel like somebody is taking me as advantage, there I will be aggressive.
- IF somebody is using my sources without permission, there I will be aggressive.

# When do you switch into Passive Aggressive 

- When somebody mislead the things or rules, instead of correcting them we keep calm by ourself.
- If somebody ask for any feedback or decisions, Instead of expressing our own feelings just giving common answer.

# How can you make your communication assertive

- Assertive communication is expressing our feelings by considering other's too.
- By giving priority to other's feelings.
- Instead of being aggressive, handle the situations in cool manner gives comfort to each other's.
- Being aggressive may lead to get anxiety, angryness. By assertive communication we can overcome these.




