# What kinds of behaviour cause sexual harassment
There are three kinds of behaviour
# Verbal harassment
- Comments about clothing.
- A person's body.
- Sexual or gender-based jokes or remarks.

# Visual harassment
- Drawing pictures, screen savers, cartoons, emails, or text of a sexual nature.

# Physical harassment
- Sexual assaults impending,
- Impending and blocking movements.
- Inappropriate touching such as kissing, hugging, patting, stroking, rubbing, etc.

# What would you do in case you face or witness any incident or repeated incidents of such behaviour

# Safety First:
- Ensure immediate safety. If in immediate danger, seek help or call emergency services.

# Document the Incident:
- Keep a record of what happened, including dates, times, locations, and details of the incidents.

# Report the Incident:
- Human Resources (if at work)
- School authorities (if at school or college)
- Law enforcement (if a crime has been committed)

# Seek Support:
- Talk to a trusted friend, family member, or counselor for emotional support.
- Seek guidance from organizations specializing in handling sexual harassment cases.

# Know Your Rights:
- Familiarize yourself with laws and policies regarding sexual harassment in your area.

# Encourage Others to Report:
- If you witness such behavior, encourage the affected individual to report the incident and offer support.