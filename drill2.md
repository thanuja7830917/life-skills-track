# What is the Feynman Technique
 If we can't explain in simple way, we don't understand it properly.
 There is 4 steps to implement:
 1. Notedown the topic.
 2. Explain it in a simple way.
 3. Identify the problems and solve them.
 4. Simplify the complicated terms.

# In this video, what was the most interesting story or idea for you

The most useful and interesting techniques shared by her was time management and how to be consistence. we can focus on the things by taking the short breaks while in once. Through this we can increase our efficiency.She became an expert by facing the lot of struggles.

# What are active and diffused modes of thinking

1. Active mode : 
Focusing on specific areas helps to understand the things more deeper.
2. Diffused mode :
Not Focusing on the things in efficient manner. Thinking in a relaxed mode.

# what are the steps to take when approaching a new topic

1. Deconstruct the skill
2. Learn enough to self-correct
3. Remove practice barriers
4. Practice atleast 20 hours

# What are some of the actions you can take going forward to improve your learning process

1. Removing the practice barriers. Because with more practice we become more efficient on that topic.
2. Practicing the things more.
3. Scheduling the practice time based on the knowledge of the topic we have.




