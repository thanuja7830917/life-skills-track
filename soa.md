# Service Oriented Architecture :

# Defining SOA :

Service oriented architecture is a method of Software development that uses services to create Web Applications.

# Uses of SOA and how it will works :

1. Thorugh this SOA we can reuse the services to develop applications.

2. For example, there is a company like used to create different type of web applications ,in that applications we should implement a Authentication ,we need 
 to write the code repeatedly. Instead of that we can create a single Authentication service and we can reuse that service for all applications.

3. Developers can save the time by using these services.

4.Organizations can create their own services. 

# How this SOA works :

1. The consumer requests information and sends an input data to service.It will process the data and perform the task and gives the response.

2. Assume,we are using the Authorization service to implement a authorization in our web application. It will take the inputs as username,password
and verifies the data and returns appropriate response.

# Service Provider :

1. The one who maintains service and the organization that that makes available one or more services for others to use.

# Service Consumer :

1. The one who uses the service .

# Advantages :

-  Easy maintenance : It is an independent so,easy to maintain.

-  Reusability : we can reuse the the services in applications .

-  Scalability : It will run accross multiple platforms .

-  Reliability : with the small and independent services ,it becomes easier to test and debug .

# Disadvantages :

-  It is expensive in terms of human resources.


# References

youtube : [https://www.youtube.com/watch?v=PA9RjHI463g]

google : [https://www.geeksforgeeks.org/service-oriented-architecture/]


